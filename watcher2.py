import os
import sys
import glob
import time
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

name_initial = '+55 12 98117-4661' #'Marcio Ittalli'
chrome_dir = '/home/eduardo/chrome2/'

def sendMessage(msg, index):
    web_obj = driver.find_element_by_xpath("//div[@contenteditable='true']")

    if type(msg) is list:
        web_obj.send_keys(msg[index])
    else:
        web_obj.send_keys(msg)

    web_obj.send_keys(Keys.RETURN)


def main():

    options = Options()
    # options.add_argument('--headless')
    options.add_argument(f'--user-data-dir={chrome_dir}.')
    # options.add_argument('--proxy-server=127.0.0.1:7777')
    driver = webdriver.Chrome( options=options)
    driver.get('https://web.whatsapp.com/');
    name = name_initial

    WebDriverWait(driver, 40).until(
                EC.presence_of_element_located((By.XPATH, "//span[@title='{}']".format(name))))
    driver.find_elements_by_xpath("//span[@title='{}']".format(name))[0].click()
    is_online = False
    while True:

        name = driver.find_elements_by_xpath("//div[@class='_2EbF-']")[0].text # _2zCDG _2EbF-  _1WBXd  _1wjpf

        watcher = driver.find_elements_by_xpath("//div[@class='_1WBXd']")[0].text
        if 'en ligne' in watcher or 'en train de' in watcher or 'online' in watcher or 'digitando' in watcher:
            if is_online == False:
                os.system("notify-send \"{name} Online\"".format(name=name))
                #if name_initial == name:
                #    sendMessage(msg, 1)
            is_online = True 
            print('on ', end='')
        else: 
            if is_online == True:
                os.system("notify-send \"{name} Offline\"".format(name=name))
            is_online = False
            print('off ', end='')
        sys.stdout.flush()
        time.sleep(1)

def cleanFiles():
    # get a recursive list of file paths that matches pattern including sub directories
    fileList = glob.glob(f'{chrome_dir}/**/*onfli*', recursive=True)
     
    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            print('Removing ', filePath)
            os.remove(filePath)
        except OSError:
            print("Error while deleting file", filePath)



try:
    cleanFiles()
    main()

except Exception as e:
    try:
        driver.close()
    except:
        pass
    print("Erro - {}. Reabrindo o navegador ".format(e))
    main()

